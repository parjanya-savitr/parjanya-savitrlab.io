+++
title = "Parjanya-Savitr"
description = "Rain-cloud belonging to the rising Sun."
+++

This website will be primarily about Hinduism. [^1]

[^1]: 
    the word Hinduism is a bit of a misnomer, the popularly accepted phrase is Sanatana Dharma.

    
This work is licensed under a [CC BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/) unless mentioned otherwise.
